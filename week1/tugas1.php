<?php
// echo
$a = "hello ";
$b = "world";

echo $a.$b;
echo "<br>";
echo $a,$b;

// print
$txt1 = "Belajar PHP";
$txt2 = "Fakultas Ilmu Komputer";
$x = 5;
$y = 4;
$ret=print ("Hello World");
print "nilai return = $ret";
print "<h2>" . $txt1 . "</h2>";
print "Belajar  Pemrograman Web  di " . $txt2 . "<br>";
print $x + $y;
echo "<br>";

// print_r
$siswa = ['Arif', 'Beta', 'Cici']; 
echo '<pre>';
print_r($siswa);
echo '</pre>'; 
echo "<br>";

// var_dump
$nama = 'Agus';
var_dump ($nama); // Hasil: string(4) "Agus“

$siswa = array(
	'nama' => array ('Arif', 'Beta', 'Cici'),
	'jurusan' 	=> 'Informatika',
	'semester'=> 1,
	1 => 'Jakarta',
	2 => 'Surabaya'
	);
		 
echo '<pre>';  var_dump($siswa); echo '</pre>';
echo "<br>";

//comments
//ini teks yg tdk dieksekusi oleh engine php
/*
Ini adalah blok 
baris 
komentar 
*/
echo "<br>";

// Variable lokal
function fungsi1() {
    $x = 5; // local scope
    echo "<p>Variable x  di dlm fungsi: $x</p>";
  } 
  fungsi1();
  // menggunakan  variabel x di luar fungsi  akan mengakibatkan error
  echo "<p>Variable x di luar fungsi : $x</p>";
  echo "<br>";

  //Variabel Global
$x = 5;
$y = 10;

function fungsi2() {
   $GLOBALS['y'] = $GLOBALS['x'] + $GLOBALS['y'];
  
}

fungsi2();
echo $y; // hasil = 15
echo "<br>";

// Variabel static 
function fungsi3() {
    static $x = 0;
    echo $x;
    $x++;
  }
  
  fungsi3();
  fungsi3();
  fungsi3();
  echo "<br>";
  

// Tipe Data - String
$x = "Hello world!";
$y = 'Hello world!';

echo $x;
echo "<br>";
echo $y;
echo "<br>";

//Tipe Data - integer
$x = 5985;
var_dump($x);
echo "<br>";

//Tipe Data - float
$x = 10.365;
var_dump($x);
echo "<br>";

//Tipe Data - Boolean
$x = true;
$y = false;
var_dump($x);
echo "<br>";

//Tipe Data - Array
$mobil = array("Mitsubishi","Daihatsu","Toyota");
var_dump($mobil);
echo "<br>";

//Tipe Data - Object
class Car{
public $color;
public $model;
public function __construct($color, $model) {
  $this->color = $color;
  $this->model = $model;
}
public function message() {
  return "My car is a " . $this->color . " " . $this->model . "!";
}
}

$myCar = new Car("black", "Volvo");
echo $myCar -> message();
echo "<br>";
$myCar = new Car("red", "Toyota");
echo $myCar -> message();
echo "<br>";

//Tipe Data - Null 
$x = "Hello world!";
$x = null;
var_dump($x);
echo "<br>";

//Konstanta
define("SALAM", "Selamat Pagi");

define("mobil", [
  "Honda",
  "Daihatsu",
  "Toyota"
]);

function fungsi4() {
  echo  SALAM;
}
 
fungsi1();
echo mobil[0];



?>