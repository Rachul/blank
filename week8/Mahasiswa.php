<?php
   
    class Mahasiswa
    {
        public $nim;
        public $nama;
        public $alamat;
        public $prodi;


        function setData($nim,$nama,$alamat,$prodi)
        {
            $this->nim=$nim;
            $this->nama=$nama;
            $this->alamat=$alamat;
            $this->prodi=$prodi;
        }

        function cetakData()
        {
            echo "NIM : ".$this->nim."<br>";
            echo "Nama : ".$this->nama."<br>";
            echo "alamat : ".$this->alamat."<br>";
            if ($this->prodi=="A11")
            echo "Program Studi : Teknik Informatika";
            else if ($this->prodi=="A12")
            echo "Program Studi : Sistem Informasi";
            else if ($this->prodi=="B11")
            echo "Program Studi : Manajemen";
            else if ($this->prodi=="B12")
            echo "Program Studi : Akuntansi";
        }
    }
    